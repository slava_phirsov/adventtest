#include <QApplication>

#include <QList>
#include <QSet>
#include <QHash>

#include <QPoint>

#include <QPen>
#include <QBrush>

#include <QScopedPointer>

#include <QWidget>
#include <QVBoxLayout>
#include <QToolBar>
#include <QStatusBar>
#include <QFileDialog>

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsEllipseItem>

#include <QMouseEvent>

#include <QtSql>


enum {
    Width = 100, Height = 100,
    Spacing = 2,
    CellCount = Width * Height,
    ItemCount = CellCount / Spacing
};


uint qHash(const QPoint& point)
{
    return qHash(point.x()) ^ qHash(point.y());
}

QPoint left(const QPoint& point)
{
    return QPoint(point.x() - 1, point.y());
}

QPoint right(const QPoint& point)
{
    return QPoint(point.x() + 1, point.y());
}

QPoint top(const QPoint& point)
{
    return QPoint(point.x(), point.y() - 1);
}

QPoint bottom(const QPoint& point)
{
    return QPoint(point.x(), point.y() + 1);
}


class Dijkstra
{
public:
    Dijkstra(const QSet<QPoint>&);
    QList<QPoint> evaluatePath(const QPoint& , const QPoint&);

private:

    void start(const QPoint&);

    bool hasItem(const QPoint&);
    bool hasPending();
    void getNextPending();
    void reevaluate();
    void setNeighboursPending();

    QList<QPoint> getResult(const QPoint& , const QPoint&);

    QPoint current_;

    QSet<QPoint> pending_, proceeded_;
    QHash<QPoint, int> distance_;
    QHash<QPoint, QPoint> path_;

    QSet<QPoint> items_;
};

Dijkstra::Dijkstra(const QSet<QPoint>& items):
    items_(items)
{}

QList<QPoint> Dijkstra::evaluatePath(const QPoint& first, const QPoint& last)
{
    start(first);

    while (hasPending())
    {
        getNextPending();
        reevaluate();
        setNeighboursPending();
    }

    return getResult(first, last);
}

void Dijkstra::start(const QPoint& first)
{
    pending_.clear();
    proceeded_.clear();
    distance_.clear();
    path_.clear();

    pending_ += first;
    distance_[first] = 0;
    path_[first] = first;
}

bool Dijkstra::hasItem(const QPoint& pos)
{
    return items_.contains(pos);
}

bool Dijkstra::hasPending()
{
    return !pending_.isEmpty();
}

void Dijkstra::getNextPending()
{
    if (!pending_.isEmpty())
    {
        QSet<QPoint>::iterator
            min = pending_.begin(),
            i = pending_.begin();

        ++i;

        int min_distance = distance_[*min];

        for (; i != pending_.end(); ++i)
        {
            int distance = distance_[*i];

            if (distance < min_distance)
            {
                min = i;
                min_distance = distance;
            }
        }

        current_ = *min;

        pending_.erase(min);

        proceeded_ += current_;
    }
}

void Dijkstra::reevaluate()
{
    int current_distance = distance_[current_];

    foreach (const QPoint& item, pending_)
    {
        if (
            (current_ - item).manhattanLength() == 1 &&
            distance_[item] > current_distance + 1
        )
        {
            path_[item] = current_;
            distance_[item] = current_distance + 1;
        }
    }
}

void Dijkstra::setNeighboursPending()
{
    enum {Top, Left, Right, Bottom, NeighbourCount};

    QPoint neighbour[NeighbourCount] = {
        top(current_), left(current_), right(current_), bottom(current_)
    };

    int distance = distance_[current_] + 1;

    for (int i = 0; i < NeighbourCount; ++i)
    {
        const QPoint& item = neighbour[i];

        if (
            items_.contains(item) &&
            !pending_.contains(item) &&
            !proceeded_.contains(item)
        )
        {
            pending_ += item;
            distance_[item] = distance;
            path_[item] = current_;
        }
    }
}

QList<QPoint> Dijkstra::getResult(const QPoint& first, const QPoint& last)
{
    QList<QPoint> result;

    if (path_.contains(last))
    {
        for (QPoint item = last; item != first; item = path_[item])
            result += item;

        result += first;
    }

    return result;
}


QSet<QPoint> makeRandom()
{
    QSet<QPoint> result;

    QSet<int> occupied;

    occupied.reserve(ItemCount);
    result.reserve(ItemCount);

    for (int i = 0; i < ItemCount; ++i)
    {
        int j = qrand() % (CellCount), k = 0;

        while (j >= 0)
        {
            if (!occupied.contains(k))
                --j;

            k = (k + 1) % (CellCount);
        }

        occupied += k;

        result += QPoint(k % Width, k / Width);
    }

    return result;
}

QSet<QPoint> loadFromDb(const QString& filename)
{
    QSet<QPoint> result;

    QSqlDatabase database = QSqlDatabase::database("gameboard", false);

    if (database.isValid())
    {
        QSqlDriver* driver = database.driver();

        if (driver && driver->open(filename))
        {
            QSqlQuery query = database.exec("SELECT x, y FROM items");

            if (!query.lastError().isValid())
            {
                while (query.next())
                {
                    bool x_ok = false, y_ok = false;

                    int x = query.record().value("x").toInt(&x_ok),
                        y = query.record().value("y").toInt(&y_ok);

                    if (x_ok && y_ok)
                        result += QPoint(x, y);
                }
            }

            driver->close();
        }
    }

    return result;
}

void saveToDb(const QSet<QPoint>& gameboard, const QString& filename)
{
    QSqlDatabase database = QSqlDatabase::database("gameboard", false);

    if (database.isValid())
    {
        QSqlDriver* driver = database.driver();

        if (driver && driver->open(filename))
        {
            // TODO may fail in case table not exists

            database.exec("DROP TABLE items");

            database.exec(
                "CREATE TABLE items (x INT, y INT,"
                "CONSTRAINT pk_xy PRIMARY KEY (x, y))"
            );

            QSqlQuery insert(database);

            insert.prepare("INSERT INTO items (x, y) VALUES (:x, :y)");

            database.transaction();

            foreach (const QPoint& pos, gameboard)
            {
                insert.bindValue(":x", QVariant(pos.x()));
                insert.bindValue(":y", QVariant(pos.y()));

                if (!insert.exec())
                {
                    database.rollback();

                    return;
                }
            }

            database.commit();
        }
    }
}


class MainWidget: public QWidget
{
    Q_OBJECT
public:
    explicit MainWidget(QWidget* parent=0);

    virtual bool eventFilter(QObject* , QEvent*);

private slots:
    void renew();
    void load();
    void save();

private:

    enum {CellSize = 10, ItemDataKey = 0};

    void setupToolBar();
    void setupGraphicsView();
    void setupStatusBar();

    void update(QSet<QPoint>);

    void onItemClick(QGraphicsEllipseItem*);

    void selectFirst(QGraphicsEllipseItem*);
    void selectLast(QGraphicsEllipseItem*);
    void deselectAll();

    QGraphicsEllipseItem* makeItem(QGraphicsScene* , const QPoint&);
    QGraphicsItemGroup* makePath(QGraphicsScene* , const QList<QPoint>&);

    void highlight(QGraphicsEllipseItem*);
    void unhighlight(QGraphicsEllipseItem*);
    void status(const QString&);

    QSet<QPoint> gameboard_;

    QToolBar* tool_bar_;
    QGraphicsView* graphics_view_;
    QGraphicsEllipseItem *first_, *last_;
    QGraphicsItemGroup* path_;
    QStatusBar* status_bar_;
};

MainWidget::MainWidget(QWidget* parent):
    QWidget(parent),
    tool_bar_(0),
    graphics_view_(0), first_(0), last_(0), path_(0),
    status_bar_(0)
{
    new QVBoxLayout(this);

    setupToolBar();
    setupGraphicsView();
    setupStatusBar();
}

void MainWidget::setupToolBar()
{
    tool_bar_ = new QToolBar(this);

    layout()->addWidget(tool_bar_);

    tool_bar_->setFloatable(false);
    tool_bar_->setMovable(false);

    tool_bar_->addAction(tr("renew"), this, SLOT(renew()));
    tool_bar_->addAction(tr("load"), this, SLOT(load()));
    tool_bar_->addAction(tr("save"), this, SLOT(save()));
}

void MainWidget::setupGraphicsView()
{
    graphics_view_ = new QGraphicsView(this);

    layout()->addWidget(graphics_view_);

    graphics_view_->setRenderHints(QPainter::Antialiasing);
    graphics_view_->installEventFilter(this);
}

void MainWidget::setupStatusBar()
{
    status_bar_ = new QStatusBar(this);

    layout()->addWidget(status_bar_);
}

bool MainWidget::eventFilter(QObject* watched, QEvent* event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent* mouse_event = static_cast<QMouseEvent*>(event);

        if (mouse_event->button() == Qt::LeftButton)
            onItemClick(
                qgraphicsitem_cast<QGraphicsEllipseItem*>(
                    graphics_view_->itemAt(mouse_event->pos())
                )
            );
    }

    return QWidget::eventFilter(watched, event);
}

void MainWidget::onItemClick(QGraphicsEllipseItem* item)
{
    if (first_ && last_)
    {
        deselectAll();
        selectFirst(item);
    }
    else if (first_ && item)
        selectLast(item);

    else if (item)
        selectFirst(item);
    else
        deselectAll();
}

void MainWidget::selectFirst(QGraphicsEllipseItem* item)
{
    highlight(item);

    first_ = item;

    if (first_)
        status(tr("Select last point"));
}

void MainWidget::selectLast(QGraphicsEllipseItem* item)
{
    highlight(item);

    last_ = item;

    if (first_ && last_)
    {
        QPoint
            first_pos = first_->data(ItemDataKey).toPoint(),
            last_pos = last_->data(ItemDataKey).toPoint();

        QList<QPoint> path =
            Dijkstra(gameboard_).evaluatePath(first_pos, last_pos);

        if (path.isEmpty())
            status(tr("Items are not connected"));
        else
        {
            path_ = makePath(graphics_view_->scene(), path);

            status(tr("Path completed"));
        }
    }
}

void MainWidget::deselectAll()
{
    unhighlight(first_);
    unhighlight(last_);

    first_ = 0;
    last_ = 0;

    QGraphicsItem* old_path = path_;

    path_ = 0;

    delete old_path;

    status(tr("Select first point"));
}

void MainWidget::renew()
{
    update(makeRandom());
}

void MainWidget::load()
{
    QString filename =
        QFileDialog::getOpenFileName(this, tr("Select file to load from"));

    if (!filename.isEmpty())
        update(loadFromDb(filename));
}

void MainWidget::save()
{
    QString filename =
        QFileDialog::getSaveFileName(this, tr("Select file to save to"));

    if (!filename.isEmpty())
        saveToDb(gameboard_, filename);
}

void MainWidget::update(QSet<QPoint> gameboard)
{
    QScopedPointer<QGraphicsScene> scene(new QGraphicsScene(this));

    foreach (QPoint pos, gameboard)
        makeItem(scene.data(), pos);

    QGraphicsScene* old_scene = graphics_view_->scene();

    graphics_view_->setScene(scene.data());

    delete old_scene;

    gameboard_.swap(gameboard);

    first_ = 0;
    last_ = 0;

    scene.take();

    status(tr("Select first point"));
}

QGraphicsEllipseItem* MainWidget::makeItem(
    QGraphicsScene* scene, const QPoint& pos
)
{
    QScopedPointer<QGraphicsEllipseItem> item(
        scene->addEllipse(
            pos.x() * CellSize - CellSize / 2.0,
            pos.y() * CellSize - CellSize / 2.0,
            CellSize,
            CellSize
        )
    );

    item->setData(ItemDataKey, pos);

    unhighlight(item.data());

    return item.take();
}

QGraphicsItemGroup* MainWidget::makePath(
    QGraphicsScene* scene,
    const QList<QPoint>& path
)
{
    if (!path.isEmpty())
    {
        QScopedPointer<QGraphicsItemGroup> result(new QGraphicsItemGroup());

        foreach (const QPoint& pos, path)
        {
            QScopedPointer<QGraphicsEllipseItem> segment(
                scene->
                    addEllipse(
                        pos.x() * CellSize - CellSize / 4.0,
                        pos.y() * CellSize - CellSize / 4.0,
                        CellSize / 2.0,
                        CellSize / 2.0,
                        QPen(Qt::NoPen),
                        QBrush(Qt::blue)
                    )
            );

            result->addToGroup(segment.take());
        }

        scene->addItem(result.data());

        return result.take();
    }

    return 0;
}

void MainWidget::highlight(QGraphicsEllipseItem* item)
{
    if (item)
        item->setBrush(Qt::red);
}

void MainWidget::unhighlight(QGraphicsEllipseItem* item)
{
    if (item)
        item->setBrush(Qt::green);
}

void MainWidget::status(const QString& message)
{
    status_bar_->showMessage(message);
}


int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    QSqlDatabase::addDatabase("QSQLITE", "gameboard");

    MainWidget main_widget;

    main_widget.showMaximized();

    return app.exec();
}


#include "main.moc"
