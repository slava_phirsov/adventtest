# README / ЧАВО #

Test project for one small IT-company.

Тестовое задание, выполненное для одной компании.

### What is this repository for? / Что тут к чему ###

Qt desktop application. Given the maze consists of random points. User selects two points by mouse click. Application plots the shortest path between theese points, if one exists. The maze can be created by application, loaded from / saved to the SQLite database file.

Qt-приложение. Имеется множество случайно расположенных точек. Пользователь выбирает кликом мыши две любые точки, приложение строит кратчайший путь между этими точками (если таковой существует). Приложение может само строить набор точек, загружать его из базы данных или сохранять в базу данных. В качестве движка базы данных используется SQLite.

### Contribution guidelines / Как бы помочь ###

Do nothing.

Просто не мешайте. Этот проект сделан "для себя", в чисто познавательных целях.
